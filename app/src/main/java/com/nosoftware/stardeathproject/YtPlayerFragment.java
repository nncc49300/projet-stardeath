package com.nosoftware.stardeathproject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.support.v4.app.Fragment;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Created by Nicolas on 25/01/2018.
 */

public class YtPlayerFragment extends android.app.Fragment {
    Button b;
    private YouTubePlayerView ytpView;
    private YouTubePlayer.OnInitializedListener onInitializedListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_youtube_layout, container, false);
        ytpView = view.findViewById(R.id.playerView);
        b = view.findViewById(R.id.playButton);

        String idVideo;
        Bundle args = getArguments();
        if(args == null){
            idVideo = "dQw4w9WgXcQ";
        }else
        {
            idVideo = args.getString("idVideo", "dQw4w9WgXcQ");
        }


        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(idVideo);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };


        b.setOnClickListener(v -> {
            ytpView.initialize("AIzaSyC0SPBRwNWnz5uJM-EpRJNTTFTTXyR7GuI",onInitializedListener);
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



    }
}
