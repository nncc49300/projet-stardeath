package com.nosoftware.stardeathproject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeBaseActivity;

/**
 * Created by Nicolas on 25/01/2018.
 */

public class YoutubeListActivity extends YouTubeBaseActivity{
    TextView tv;
    TextView titleTv;
    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_list);


        Intent i = getIntent();
        String latLng = i.getStringExtra("LatLng");
        titleTv = findViewById(R.id.textViewTitle);
        rv = findViewById(R.id.recyclerList);
        rv.setLayoutManager(new LinearLayoutManager(this));

        Fragment fr;
        fr = new YtPlayerFragment();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.playerYtFragment, fr);

        fragmentTransaction.commit();


        titleTv.setText(latLng);

        rv.setAdapter(new MyAdapter());

    }
    public void onClickRecycler(View v){
        Fragment fr;
        fr = new YtPlayerFragment();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.playerYtFragment, fr);

        fragmentTransaction.commit();
    }

}
