package com.nosoftware.stardeathproject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button buttonGoogle;
    Button buttonFrag1;
    Button buttonFrag2;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonGoogle = findViewById(R.id.buttonGoogle);
        buttonFrag1 = findViewById(R.id.buttonFrag1);
        buttonFrag2 = findViewById(R.id.buttonFrag2);


        buttonGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(getApplicationContext(), MapsMarkerActivity.class);
                startActivity(i);
            }
        });

        buttonFrag1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fr;
                fr = new FragmentTest1();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.testLayoutFragment, fr);
                fragmentTransaction.commit();
            }
        });

        buttonFrag2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fr;
                fr = new FragmentTest2();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.testLayoutFragment, fr);
                fragmentTransaction.commit();
            }
        });
    }

}


