package com.nosoftware.stardeathproject;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private YtPlayerFragment ytPlayerFragment = new YtPlayerFragment();
    // get fragment manager



    private final List<Pair<String, String>> characters = Arrays.asList(
            Pair.create("Never Gonna Give You Up","dQw4w9WgXcQ"),
            Pair.create("RAW Ruthless Aggression Theme Song","WqeQKbkAZwE"),
            Pair.create("TheFatRat - Unity ","n8X9_MgEdCg"),
            Pair.create("Initial D - Deja Vu ","dv13gl0a-FA"),
            Pair.create("No Game No Life Opening Full","CaksNlNniis"),
            Pair.create("Jump Up, Super Star! (Full Ver. Official iTunes Release) Super Mario Odyssey Main Theme","PhciLj5VzOk"),
            Pair.create("alphabet shuffle","g02WKrWjUgA"),
            Pair.create("I'll Make A Man Out Of You but every s is emphasized plus zooms","dQw4w9WgXcQ"),
            Pair.create("[YTP FR] - L' ATTENTAT DU POUDLARD EXPRESS ","_BhS_sGVkcw")

    );

    @Override
    public int getItemCount() {
        return characters.size();
    }

    @Override

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_cell, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pair<String, String> pair = characters.get(position);
        holder.display(pair);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView textViewTitreVideo;
        private final TextView textViewIDVideo;
        private Pair<String, String> currentPair;


        public MyViewHolder(final View itemView) {
            super(itemView);
            textViewTitreVideo = ((TextView) itemView.findViewById(R.id.textViewTitreVideo));
            textViewIDVideo = ((TextView) itemView.findViewById(R.id.textViewIDVideo));



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Bundle args = new Bundle();
                    args.putString("title", currentPair.first);
                    args.putString("idVideo", currentPair.second);
                    ytPlayerFragment.setArguments(args);

                    //YoutubeListActivity youtubeListActivity = new YoutubeListActivity();
                    //youtubeListActivity.onClickRecycler(view);

                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle("Affichage de la Video")
                            .setMessage("Titre : " + currentPair.first)
                            .show();
                }
            });

        }

        void display(Pair<String, String> pair) {
            currentPair = pair;
            textViewTitreVideo.setText(pair.first);
            textViewIDVideo.setText(pair.second);
        }



    }

}